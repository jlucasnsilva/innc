/** \file */
/*
    Copyright (c) 2017 João Lucas Nunes e Silva
 
    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.
 
    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
 
    1. The origin of this software must not be misrepresented; you must not
       claim that you wrote the original software. If you use this software
       in a product, an acknowledgment in the product documentation would be
       appreciated but is not required.
    2. Altered source versions must be plainly marked as such, and must not be
       misrepresented as being the original software.
    3. This notice may not be removed or altered from any source distribution.
*/
/**
    \page argv is a tiny and simple library for handling command line flags.
          It only supports three input types: string, integer and flag.
*/
#ifndef INNC_ARGV_H_
#define INNC_ARGV_H_


#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>


/** The default 0 value for errors. */
#define ARGV_NO_ERROR 0

/** Signals that a call to malloc failed. */
#define ARGV_NOT_ENOUGH_MEMORY -1

/** Used when a flag is set to "required" but it isn't found. */
#define ARGV_FLAG_NOT_FOUND -2

/** Used when a flag is found but its value is not. */
#define ARGV_VALUE_NOT_FOUND -3

/** Used when the value passed to a flag doesn't the flag's expected type. */
#define ARGV_PARSING_ERROR -4


typedef void (* argv_ErrHandler)(const char *restrict flag, int err);


/** Initializes internal variables and allocates a buffer to keep the flags.

    \param argc the argc that is passed to main function.
    \param argv the argv that is  passed to main function.
    \param num_of_flags the maximum number of flags that the program will use.
    \param errhandler called by \ref argv_parse when an error occurs.

    \return "true" if num_of_flags >= 1, "false" otherwise.
*/
bool argv_init(int argc, char* argv[], uint32_t num_of_flags, argv_ErrHandler errhandler);

/** Frees previously allocated memory. */
void argv_quit();


/** Prints the programs usage. */
void argv_usage();

/** Parses argv and extract the flags passed to argv_{int, str, flag}. If no
    error handler was passed to argv_init the parsing will fail.

    \return false if no error handler was passed to argv_init. True otherwise.
*/
bool argv_parse();

/** Returns true if argv_parse successed.

    \return true if argv_parse successed.
*/
bool argv_parsed();


/** Returns a string to an error code.

    \return the string description to an error code.
*/
const char* argv_errstr(int err);


/** Registers a new int flag. Once argv is parsed, the value is stored in "var".
    If the number of calls to argv_{int, str, flag} is greater than "num_of_flags",
    the error ARGV_NOT_ENOUGH_MEMORY will be raised and this function won't register
    the new flag.

    \param flag the flag name expected in the command line (e.g. "--myflag" or "-myflag").
    \param var a pointer to the variable that will receive the value from the flag.
    \param not_found the default value in case the flag isn't found.
    \param required if set "true" and the flag ins't found, var will be set to the
           deafult value and the error ARGV_FLAG_NOT_FOUND will be raised.
    \param message the message printed by \ref argv_usage.
*/
void argv_int(const char* flag, int* var, int not_found, bool required, const char* message);

/** Registers a new string flag. Once argv is parsed, the value is stored in "var".
    If the number of calls to argv_{int, str, flag} is greater than "num_of_flags",
    the error ARGV_NOT_ENOUGH_MEMORY will be raised and this function won't register
    the new flag. The found value will be copied into var up to len bytes.

    \param flag the flag name expected in the command line (e.g. "--myflag" or "-myflag").
    \param var a pointer to the variable that will receive the value from the flag.
    \param len the maximum length of var.
    \param not_found the default value in case the flag isn't found.
    \param required if set "true" and the flag ins't found, var will be set to the
           deafult value and the error ARGV_FLAG_NOT_FOUND will be raised.
    \param message the message printed by \ref argv_usage.
*/
void argv_str(const char* flag, char** var, size_t len, char* not_found, bool required, const char* message);

/** Registers a new bool flag. Once argv is parsed, the value is stored in "var".
    If the number of calls to argv_{int, str, flag} is greater than "num_of_flags",
    the error ARGV_NOT_ENOUGH_MEMORY will be raised and this function won't register
    the new flag. If the value is found, var is set to "true" and "false" otherwise.

    \param flag the flag name expected in the command line (e.g. "--myflag" or "-myflag").
    \param var a pointer to the variable that will receive the value from the flag.
    \param message the message printed by \ref argv_usage.
*/
void argv_flag(const char* flag, bool* var, const char* message);

#endif /* !INNC_ARGV_H_ */
