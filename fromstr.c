/*
    Copyright (c) 2017 João Lucas Nunes e Silva
 
    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.
 
    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
 
    1. The origin of this software must not be misrepresented; you must not
       claim that you wrote the original software. If you use this software
       in a product, an acknowledgment in the product documentation would be
       appreciated but is not required.
    2. Altered source versions must be plainly marked as such, and must not be
       misrepresented as being the original software.
    3. This notice may not be removed or altered from any source distribution.
*/
#include "fromstr.h"
#include "macros.h"

#include <string.h>


// ---------------------------------------------------------
// Functions delcs -----------------------------------------


static char* _strtoword(const char *restrict s, char** saveptr, char *restrict fill, size_t flen);
static char* _strtostr(const char *restrict s, char** saveptr, char *restrict fill, size_t flen);
static char* _strwhile(const char *restrict s, const char *restrict accept, char** saveptr, char *restrict fill, size_t flen);
static char* _struntil(const char *restrict s, const char *restrict brk, char** saveptr, char *restrict fill, size_t flen);


// ---------------------------------------------------------
// Global variables ----------------------------------------


char* fromstr_spacechars   = FROMSTR_SPACE;
char* fromstr_wordfstchars = FROMSTR_WORD1;
char* fromstr_wordchars    = FROMSTR_WORD;
char* fromstr_quotechars   = FROMSTR_QUOTE;
char* fromstr_opchars      = FROMSTR_OPERATOR;


// ---------------------------------------------------------
// Inline and macro functions ------------------------------


#define isspc(c)   (strchr(fromstr_spacechars, c)   != NULL)
#define isword1(c) (strchr(fromstr_wordfstchars, c) != NULL)
#define isword(c)  (strchr(fromstr_wordchars, c)    != NULL)
#define isquote(c) (strchr(fromstr_quotechars, c)   != NULL)

static inline void _ncpy(char *restrict dest, const char *src, char quote, size_t n)
{
    for (int i = 0; i < n; i++) {
        if (*src == '\\' && *(src + 1) == quote) {
            src++;
            i++;
        }
        *dest = *src;
        dest++;
        src++;
    }
}


// ---------------------------------------------------------
// Functions -----------------------------------------------


char* strtoword(const char *restrict s, char** saveptr)
{
    return _strtoword(s, saveptr, NULL, 0);
}

char* strtostr(const char *restrict s, char** saveptr)
{
    return _strtostr(s, saveptr, NULL, 0);
}

char* strwhile(const char *restrict s, const char *restrict accept, char** saveptr)
{
    return _strwhile(s, accept, saveptr, NULL, 0);
}

char* struntil(const char *restrict s, const char *restrict brk, char** saveptr)
{
    return _struntil(s, brk, saveptr, NULL, 0);
}


bool strtonword(const char *restrict s, char** saveptr, char *restrict out, size_t maxlen)
{
    return _strtoword(s, saveptr, out, maxlen) != NULL;
}

bool strtonstr(const char *restrict s, char** saveptr, char *restrict out, size_t maxlen)
{
    return _strtostr(s, saveptr, out, maxlen) != NULL;
}

bool strnwhile(const char *restrict s, const char *restrict accept, char** saveptr, char *restrict out, size_t maxlen)
{
    return _strwhile(s, accept, saveptr, out, maxlen) != NULL;
}

bool strnuntil(const char *restrict s, const char *restrict brk, char** saveptr, char *restrict out, size_t maxlen)
{
    return _struntil(s, brk, saveptr, out, maxlen) != NULL;
}


const char* dropspaces(const char *restrict s)
{
    while(isspc(*s)) {
        s++;
    }
    return s;
}


// ---------------------------------------------------------
// "Private" Functions -------------------------------------


static char* _strtoword(const char *restrict s, char** saveptr, char *restrict fill, size_t flen)
{
    s = dropspaces(s);

    if (!isword1(*s)) {
       return NULL;
    }

    const char* end   = s;

    while (isword(*end)) {
        end++;
    }

    size_t len = end - s;
    char*  w   = NULL;
  
    if (len > 0) {
        if (fill) {
            strncpy(fill, s, flen);
            w = fill;
        } else if ((w = malloc(len + 1))) {
            strncpy(w, s, len);
        }
    }
  
    if (saveptr) {
        *saveptr = (char *) end;
    }

    return w;
}

static char* _strtostr(const char *restrict s, char** saveptr, char *restrict fill, size_t flen)
{
    s = dropspaces(s);

    if (!isquote(*s)) {
        return NULL;
    }

    const char* end = s + 1;
    char quote = *s;

    while(*end != quote && *end != '\0') {
        if (*end == '\\' && (end + 1) != '\0' && *(end + 1) == quote) {
            end++;
        }
        end++;
    }

    if (*end != quote) {
        return NULL;
    }

    size_t len = end - s;
    char*  str = NULL;
  
    if (len > 0) {
        if (fill) {
            _ncpy(fill, s + 1, quote, min(flen, len - 1));
            str = fill;
        } else if ((str = malloc(len + 1))) {
            _ncpy(str, s + 1, quote, len - 1);
        }
    }
  
    if (saveptr) {
        *saveptr = (char *) (end + 1);
    }

    return str;
}

static char* _strwhile(const char *restrict s, const char *restrict accept, char** saveptr, char *restrict fill, size_t flen)
{
    s = dropspaces(s);

    size_t len = strspn(s, accept);
    char*  spn = NULL;
  
    if (len > 0) {
        if (fill) {
            strncpy(spn, s, flen);
            spn = fill;
        } else if((spn = malloc(len + 1))) {
            strncpy(spn, s, len);
        }
    }
  
    if (saveptr) {
        *saveptr = (char *) &s[len];
    }

    return spn;
}

static char* _struntil(const char *restrict s, const char *restrict brk, char** saveptr, char *restrict fill, size_t flen)
{
    s = dropspaces(s);

    const char* end = strpbrk(s, brk);
    size_t len = end ? end - s : 0;
    char*  str = NULL;

    if (len > 0) {
        if (fill) {
            strncpy(str, s, flen);
            str = fill;
        } else if ((str = malloc(len + 1))) {
            strncpy(str, s, len);
        }
    }
  
    if (saveptr) {
        *saveptr = (char *) end;
    }

    return str;
}
