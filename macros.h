/*
    Copyright (c) 2017 João Lucas Nunes e Silva
 
    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.
 
    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
 
    1. The origin of this software must not be misrepresented; you must not
       claim that you wrote the original software. If you use this software
       in a product, an acknowledgment in the product documentation would be
       appreciated but is not required.
    2. Altered source versions must be plainly marked as such, and must not be
       misrepresented as being the original software.
    3. This notice may not be removed or altered from any source distribution.
*/
#ifndef INNC_MACROS_H_
#define INNC_MACROS_H_


#include <stdint.h>
#include <stdlib.h>




// ---------------------------------------------------------
// Allocation Macros ---------------------------------------


#define newblock(T, size) ((T *) malloc(sizeof(T) * size))
#define new(T)            ((T *) malloc(sizeof(T)))

#define delete(ptr) { \
    if (ptr) {        \
          free(ptr);  \
          ptr = NULL; \
        }             \
    }


// ---------------------------------------------------------
// Cast Macros ---------------------------------------------


// Numerical casts
#define Char(n)    ((char) (n))
#define Short(n)   ((short) (n))
#define Int(n)     ((int) (n))
#define Long(n)    ((long) (n))
#define Int8(n)    ((int8_t) (n))
#define Int16(n)   ((int16_t) (n))
#define Int32(n)   ((int32_t) (n))
#define Int64(n)   ((int64_t) (n))
#define UChar(n)   ((unsigned char) (n))
#define UInt8(n)   ((uint8_t) (n))
#define UInt16(n)  ((uint16_t) (n))
#define UInt32(n)  ((uint32_t) (n))
#define UInt64(n)  ((uint64_t) (n))
#define Float(n)   ((float) (n))
#define Double(n)  ((double) (n))
#define Ptr(p)     ((void*) (p))


// ---------------------------------------------------------
// Other ---------------------------------------------------


#define min(x, y)  (((x) > (y)) ? (y) : (x))
#define max(x, y)  (((x) > (y)) ? (x) : (y))
#define utf8len(c) (UChar(c) >= 0xF0 ? 4 : \
                    UChar(c) >= 0xE0 ? 3 : \
                    UChar(c) >= 0xC0 ? 2 : 1)


#endif // !INNC_MACROS_H_
