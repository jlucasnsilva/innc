/*
    Copyright (c) 2017 João Lucas Nunes e Silva
 
    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.
 
    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
 
    1. The origin of this software must not be misrepresented; you must not
       claim that you wrote the original software. If you use this software
       in a product, an acknowledgment in the product documentation would be
       appreciated but is not required.
    2. Altered source versions must be plainly marked as such, and must not be
       misrepresented as being the original software.
    3. This notice may not be removed or altered from any source distribution.
*/
#include "array.h"
#include "macros.h"

#include <string.h>
#include <stdbool.h>


/* RESIZE THRESHOLDS */
#define RT_1000   2
#define RT_10000  1.5
#define RT_100000 1.2

typedef struct {
    size_t obj_size;
    size_t cap;
    size_t len;
    char   array[];
} Array;


static Array* array_resize_if_needed(Array* a);


static inline Array* deref(char *restrict a)
{
    return Ptr(a - sizeof(Array));
}


void* array_create(size_t obj_size, size_t capacity)
{
    Array* a = malloc(sizeof(Array) + obj_size * capacity);

    if (!a) {
        return NULL;
    }

    *a = (Array) {
        .obj_size = obj_size,
        .cap      = capacity,
        .len      = 0,
    };

    return a->array;
}

void array_delete(void **restrict a)
{
    if (a && *a) {
        delete(*a);
    }
}

size_t array_cap(void *restrict a)
{
    if (a) {
        return deref(a)->cap;
    }

    return 0;
}

size_t array_len(void *restrict a)
{
    if (a) {
        return deref(a)->len;
    }

    return 0;
}

void* array_append(void *restrict a, void *restrict value)
{
    if (a && value) {
        Array* arr = deref(a);
        char  (* p)[arr->obj_size] = NULL;

        arr->len++;
        arr = array_resize_if_needed(arr);
        p   = Ptr(arr->array);

        memcpy(&p[arr->len], value, arr->obj_size);

        return arr->array;
    }

    return a;
}

void* array_inc(void *restrict a)
{
    if (a) {
        Array* arr = deref(a);
        
        arr->len++;
        arr = array_resize_if_needed(arr);

        return arr->array;
    }

    return a;
}

void array_dec(void *restrict a)
{
    if (a) {
        Array* arr = deref(a);

        if (arr->len > 0) {
            --arr->len;
        }
    }
}

size_t array_objsize(void *restrict a)
{
    if (a) {
        return deref(a)->obj_size;
    }

    return 0;
}

static Array* array_resize_if_needed(Array* a)
{
    const size_t MINIMUM_SIZE = (size_t) 100;
    size_t       new_cap      = 0;
    Array*       new_array    = NULL;

    /* INFERED arr != NULL */
    if (a->len < a->cap) {
        return a;
    }

    if (a->cap == 0) {
        new_cap = max(a->len, MINIMUM_SIZE);
    } else if (a->cap <= 1000) {
        new_cap = max(a->len, RT_1000 * a->cap);
    } else if (a->cap <= 10000) {
        new_cap = max(a->len, RT_10000 * a->cap);
    } else { /* arr->cap <= RT_100000 */
        new_cap = max(a->len, RT_100000 * a->cap);
    }

    new_array = realloc(a, sizeof(Array) + a->obj_size * new_cap);

    if (new_array) {
        new_array->cap = new_cap;
        return new_array;
    }

    return a;
}
