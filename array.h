/*
    Copyright (c) 2017 João Lucas Nunes e Silva
 
    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.
 
    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
 
    1. The origin of this software must not be misrepresented; you must not
       claim that you wrote the original software. If you use this software
       in a product, an acknowledgment in the product documentation would be
       appreciated but is not required.
    2. Altered source versions must be plainly marked as such, and must not be
       misrepresented as being the original software.
    3. This notice may not be removed or altered from any source distribution.
*/
#ifndef INNC_ARRAY_H_
#define INNC_ARRAY_H_


#include <stdlib.h>


#define array_new(T, size) ((T *) array_create(sizeof(T), size))
void* array_create(size_t obj_size, size_t capacity);
void array_delete(void **restrict a);

size_t array_cap(void *restrict a);
size_t array_len(void *restrict a);

void* array_append(void *restrict a, void *restrict value);

void* array_inc(void *restrict a);
void array_dec(void *restrict a);

size_t array_objsize(void *restrict a);


#endif // !INNC_ARRAY_H_
