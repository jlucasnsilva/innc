/*
    Copyright (c) 2017 João Lucas Nunes e Silva
 
    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.
 
    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
 
    1. The origin of this software must not be misrepresented; you must not
       claim that you wrote the original software. If you use this software
       in a product, an acknowledgment in the product documentation would be
       appreciated but is not required.
    2. Altered source versions must be plainly marked as such, and must not be
       misrepresented as being the original software.
    3. This notice may not be removed or altered from any source distribution.
*/
#include "argv.h"
#include "macros.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


// ---------------------------------------------------------
// Types ---------------------------------------------------


typedef enum {
  ValueType_INT,
  ValueType_STR,
  ValueType_FLAG,
} ValueType;

typedef struct {
  const char* flag;
  const char* message;
  union {
    int*   integer;
    bool*  flag;
    char** str;
  } ptr;
  union {
    int         integer;
    const char* str;
  } not_found;
  size_t    str_len;
  ValueType type;
  bool      required;
} Flag;


// ---------------------------------------------------------
// GLOBAL VARIABLES ----------------------------------------


static char* ValueType_str[] = {
  [ValueType_INT]  = "int",
  [ValueType_STR]  = "string",
  [ValueType_FLAG] = "flag"
};

static char* required_str[] = {
  [false] = "optional",
  [true]  = "required"
};

const char* error_str[] = {
  [(-1) * ARGV_NO_ERROR]        = "no error",
  [(-1) * ARGV_FLAG_NOT_FOUND]  = "flag not found",
  [(-1) * ARGV_VALUE_NOT_FOUND] = "value not found",
  [(-1) * ARGV_PARSING_ERROR]   = "parsing error"
};

static int             argc;
static char**          argv;
static argv_ErrHandler errhandler;
static Flag*           flags;
static uint32_t        flag_count;
static uint32_t        max_flags;
static bool            parsed;


// ---------------------------------------------------------
// Func Decls ----------------------------------------------


static void parse_int(Flag *restrict f);
static void parse_str(Flag *restrict f);
static void parse_flag(Flag *restrict f);

static int find_flag(const char *restrict flag);


// ---------------------------------------------------------
// Functions -----------------------------------------------


bool argv_init(int _argc, char* _argv[], uint32_t _num_of_flags, argv_ErrHandler _errhandler) {
    if (_num_of_flags < 1) {
        return false;
    }

    argc       = _argc;
    argv       = _argv;
    errhandler = _errhandler;
    flags      = newblock(Flag, _num_of_flags);
    flag_count = 0;
    max_flags  = _num_of_flags;
    parsed     = false;

    return true;
}

void argv_quit() {
    delete(flags);
    parsed = false;
}

void argv_usage() {
    printf("%s usage:\n\n", argv[0]);

    puts("--<flag> [type][required or optional] <default value>\n  <description message>\n");

    for (int i = 0; i < flag_count; i++) {
        Flag* f = &flags[i];
        printf("\t--%-20s [%-6s] [%-8s] ", f->flag, ValueType_str[f->type], required_str[f->required]);

        switch(f->type) {
        case ValueType_INT:  printf("%d", f->not_found.integer); break;
        case ValueType_STR:  printf("\"%s\"", f->not_found.str); break;
        case ValueType_FLAG: break;
        }

        printf("\n\t  %s\n", f->message);
    }
}

bool argv_parse() {
    if (!errhandler) {
        return false;
    }

    for (uint32_t i = 0; i < flag_count; i++) {
        switch (flags[i].type) {
        case ValueType_INT:
            parse_int(&flags[i]);
            break;
        case ValueType_STR:
            parse_str(&flags[i]);
            break;
        case ValueType_FLAG:
            parse_flag(&flags[i]);
            break;
        }
    }

    parsed = true;
    return true;
}

bool argv_parsed() {
    return parsed;
}

const char* argv_errstr(int err) {
    switch (err) {
    case ARGV_NO_ERROR:
        return error_str[(-1) * err];
    case ARGV_NOT_ENOUGH_MEMORY: 
        return error_str[(-1) * err];
    case ARGV_FLAG_NOT_FOUND: 
        return error_str[(-1) * err];
    case ARGV_VALUE_NOT_FOUND: 
        return error_str[(-1) * err];
    case ARGV_PARSING_ERROR: 
        return error_str[(-1) * err];
    }
    return NULL;
}

void argv_int(const char* flag, int* var, int not_found, bool required, const char* message) {
    if (flag_count >= max_flags) {
        errhandler(flag, ARGV_NOT_ENOUGH_MEMORY);
        return;
    }

    flags[flag_count] = (Flag) {
        .flag              = flag,
        .message           = message,
        .ptr.integer       = var,
        .not_found.integer = not_found,
        .type              = ValueType_INT,
        .required          = required
    };

    *flags[flag_count++].ptr.integer = not_found;
}

void argv_str(const char* flag, char** var, size_t len, char* not_found, bool required, const char* message) {
    if (flag_count >= max_flags) {
        errhandler(flag, ARGV_NOT_ENOUGH_MEMORY);
        return;
    }
  
    flags[flag_count] = (Flag) {
        .flag          = flag,
        .message       = message,
        .ptr.str       = var,
        .not_found.str = not_found,
        .type          = ValueType_STR,
        .required      = required,
        .str_len       = len
    };

    strncpy(*flags[flag_count].ptr.str, not_found, len);
    flag_count++;
}

void argv_flag(const char* flag, bool* var, const char* message) {
    if (flag_count >= max_flags) {
        errhandler(flag, ARGV_NOT_ENOUGH_MEMORY);
        return;
    }

    flags[flag_count] = (Flag) {
        .flag          = flag,
        .message       = message,
        .ptr.flag      = var,
        .type          = ValueType_FLAG,
    };

    *flags[flag_count++].ptr.flag = false;
}


// ---------------------------------------------------------
// Internal Functions --------------------------------------


#define valid_or_return(flag, index) \
    if (!valid(flag, index)) {       \
        return;                      \
    }

static inline bool valid(Flag *restrict f, int index) {
    if (index >= 0 && index < (argc - 1)) {
        return true;
    }

    if (index >= (argc - 1)) {
        errhandler(f->flag, ARGV_VALUE_NOT_FOUND);
    } else if (f->required && index < 0) {
        errhandler(f->flag, ARGV_FLAG_NOT_FOUND);
    }
   
    return false;
}

static void parse_int(Flag *restrict f) {
    int i = find_flag(f->flag);
  
    valid_or_return(f, i);

    char* pcheck = NULL;
    int   v      = (int) strtol(argv[i + 1], &pcheck, 10);
    char  ncheck[129];

    if (pcheck == argv[i + 1]) {
        errhandler(f->flag, ARGV_PARSING_ERROR);
        return;
    }
  
    snprintf(ncheck, 128, "%d", v);
    if (strcmp(ncheck, argv[i + 1]) != 0) {
        errhandler(f->flag, ARGV_PARSING_ERROR);
        return;
    }

    *f->ptr.integer = v;
}

static void parse_str(Flag *restrict f) {
    int i = find_flag(f->flag);

    valid_or_return(f, i);

    strncpy(*f->ptr.str, argv[i + 1], f->str_len);
}

static void parse_flag(Flag *restrict f) {
    int i = find_flag(f->flag);

    if (i >= 0) {
        *f->ptr.flag = true;
    }
}

static int find_flag(const char *restrict flag) {
    int i = 0;
    char f1[65];
    char f2[65];
  
    for (i = 1; i < argc; i++) {
        snprintf(f1, 64, "-%s", flag);
        snprintf(f2, 64, "--%s", flag);
        if (strcmp(f1, argv[i]) == 0 || strcmp(f2, argv[i]) == 0) {
            return i;
        }
    }

    return -1;
}
