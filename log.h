/*
    Copyright (c) 2017 João Lucas Nunes e Silva
 
    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.
 
    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
 
    1. The origin of this software must not be misrepresented; you must not
       claim that you wrote the original software. If you use this software
       in a product, an acknowledgment in the product documentation would be
       appreciated but is not required.
    2. Altered source versions must be plainly marked as such, and must not be
       misrepresented as being the original software.
    3. This notice may not be removed or altered from any source distribution.
*/
#ifndef INNC_LOG_H_
#define INNC_LOG_H_

#include "macros.h"

#include <stdbool.h>


typedef enum {
    log_Flag_DATE = 1,
    log_Flag_TIME = 1 << 1,
    log_Flag_FILE = 1 << 2,
    log_Flags     = log_Flag_DATE | log_Flag_TIME | log_Flag_FILE
} log_Flag;

typedef struct {
    int   flags;
    bool  active;
    char  prefix[24];
} Logger;


void log_fmtd(const Logger *restrict logger, const char *restrict fmt, ...);
Logger log_make(int flags, bool active, const char *restrict prefix);


#endif /* !INNC_LOG_H_ */
