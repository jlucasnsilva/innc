/*
    Copyright (c) 2017 João Lucas Nunes e Silva
 
    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.
 
    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
 
    1. The origin of this software must not be misrepresented; you must not
       claim that you wrote the original software. If you use this software
       in a product, an acknowledgment in the product documentation would be
       appreciated but is not required.
    2. Altered source versions must be plainly marked as such, and must not be
       misrepresented as being the original software.
    3. This notice may not be removed or altered from any source distribution.
*/
#include "strbuf.h"
#include "macros.h"

#include <stdbool.h>
#include <string.h>


struct Strbuf {
    char* bs;    // the bytes
    int   cap;   // capacity in byte
    int   len;   // number of runes
    int   count; // length of bytes
};


static inline bool strbuf_iscont(const char *restrict cs, int count) {
    for (int i = 0; i < count; i++) {
        if ((cs[i] & 0xC0) != 0x80) {
            return false;
        }
    }
    return true;
}

static inline int strbuf_u8strlen(const char *restrict s)
{
    int len  = 0;
    int slen = strlen(s);

    for (int i = 0; i < slen; i += utf8len(s[i])) {
        len++;
    }

    return len;
}

static inline bool strbuf_grow(Strbuf *restrict sb)
{
    char* nbs = NULL;
    int   cap = sb->cap;

    if (cap < 1000) {
        cap *= 2;
    } else if (cap < 10000) {
        cap *= 1.5;
    } else {
        cap *= 1.2;
    }

    if (!(nbs = realloc(sb->bs, cap))) {
        return false;
    }

    sb->bs = nbs;
    return true;
}


Strbuf* strbuf_new(int cap)
{
    Strbuf* sb     = NULL;
    char*   buffer = NULL;

    if (cap < 1) {
        return NULL;
    }

    if (!(sb = new(Strbuf))) {
        return NULL;
    }

    if (!(buffer = malloc(cap))) {
        delete(sb);
        return NULL;
    }

    sb->bs    = buffer;
    sb->cap   = cap;
    sb->len   = 0;
    sb->count = 0;

    return sb;
}

void strbuf_delete(Strbuf ** sb)
{
    if (sb && *sb) {
        delete(*sb);
    }
}

int strbuf_len(const Strbuf *restrict sb)
{
    return sb ? sb->len : 0;
}

int strbuf_cap(const Strbuf *restrict sb)
{
    return sb ? sb->cap : 0;
}

int strbuf_count(const Strbuf *restrict sb)
{
    return sb ? sb->count : 0;
}

int64_t strbuf_get(Strbuf *restrict sb, int i)
{
    int64_t w    = 0;
    int     j    = 0;
    int     rlen = 0;

    if (sb && i >= sb->len) {
        return -1;
    }

    j = strbuf_idx(sb, i);

    rlen = utf8len(sb->bs[j]);
    if (rlen == 4) {
        w |= UInt32(sb->bs[j++]) << 24;
    }
    if (rlen >= 3) {
        w |= UInt32(sb->bs[j++]) << 16;
    }
    if (rlen >= 2) {
        w |= UInt32(sb->bs[j++]) << 8;
    }
    if (rlen >= 1) {
        w |= UInt32(sb->bs[j++]);
    }
    return w;
}

const char* strbuf_bytes(Strbuf *restrict sb)
{
    return sb ? sb->bs : 0;
}

int strbuf_add(Strbuf *restrict sb, const char *restrict bytes, int at)
{
    int bytes_len = strlen(bytes);

    if (strlen(bytes) != strbuf_valid(bytes)) {
        return -1;
    }
    if (sb->cap < (sb->count + bytes_len)) {
        strbuf_grow(sb);
    }

    at = strbuf_idx(sb, at);

    // make space
    memmove(&sb->bs[at + bytes_len], &sb->bs[at], sb->count - at);
    // insert
    memcpy(&sb->bs[at], bytes, bytes_len);

    sb->count += bytes_len;
    sb->len   += strbuf_u8strlen(bytes);

    sb->bs[sb->count] = '\0';
    return 0;
}

int strbuf_rm(Strbuf *restrict sb, int at, int count)
{
    int chunk_len = 0;

    at = strbuf_idx(sb, at);
    
    for (int i = at, c = 0; c < count && i < sb->count; c++) {
        chunk_len += utf8len(sb->bs[i]);
        i += utf8len(sb->bs[i]);
    }

    // the +1 at the end is so the '\0' is copied too
    memmove(&sb->bs[at], &sb->bs[at + chunk_len], sb->count - at - chunk_len + 1);

    sb->len--;
    sb->count -= chunk_len;
    return 0;
}

int strbuf_valid(const char *restrict s)
{
    size_t len   = strlen(s);
    int    valid = 0;

    while (s[valid] && (s[valid] & 0xC0) != 0x80) {
        if ((s[valid] & 0x80) == 0x00) {
            valid++;
        } else if ((s[valid] & 0xE0) == 0xC0 && (valid + 1) < len && strbuf_iscont(&s[valid + 1], 1)) {
            valid += 2;
        } else if ((s[valid] & 0xF0) == 0xE0 && (valid + 2) < len && strbuf_iscont(&s[valid + 1], 2)) {
            valid += 3;
        } else if ((s[valid] & 0xF8) == 0xF0 && (valid + 3) < len && strbuf_iscont(&s[valid + 1], 3)) {
            valid += 4;
        } else {
            return valid;
        }
    }

    return valid;
}

int strbuf_idx(Strbuf *restrict sb, int i)
{
    int j;
    int c;
    for (j = 0, c = 0; c != i; c++, j += utf8len(sb->bs[j])) {
        // do nothing
    }
    return j;
}

StrbufBreak strbuf_break(Strbuf *restrict sb, int at)
{
    StrbufBreak sbr = {NULL, NULL};

    if (!sb || at < 1 || at > (sb->len - 2)) {
        return sbr;
    }

    at = strbuf_idx(sb, at);

    if (!(sbr.right = strbuf_new(sb->count - at + 1))) {
        return sbr;
    }

    strcpy(sbr.right->bs, &sb->bs[at]);
    memset(&sb->bs[at], 0, sb->count - at);
    sbr.left = sb;

    sbr.left->count  = strlen(sbr.left->bs);
    sbr.right->count = strlen(sbr.right->bs);
    sbr.left->len    = strbuf_u8strlen(sbr.left->bs);
    sbr.right->len   = strbuf_u8strlen(sbr.left->bs);

    return sbr;
}
