/*
    Copyright (c) 2017 João Lucas Nunes e Silva
 
    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.
 
    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
 
    1. The origin of this software must not be misrepresented; you must not
       claim that you wrote the original software. If you use this software
       in a product, an acknowledgment in the product documentation would be
       appreciated but is not required.
    2. Altered source versions must be plainly marked as such, and must not be
       misrepresented as being the original software.
    3. This notice may not be removed or altered from any source distribution.
*/
#ifndef INNC_STRBUF_H_
#define INNC_STRBUF_H_

#include <stdint.h>
#include <stdlib.h>


typedef struct Strbuf Strbuf;

typedef struct {
    Strbuf* left;
    Strbuf* right;
} StrbufBreak;


Strbuf* strbuf_new(int cap);
void strbuf_delete(Strbuf ** sb);

int strbuf_len(const Strbuf *restrict sb);
int strbuf_cap(const Strbuf *restrict sb);
int strbuf_count(const Strbuf *restrict sb);

int64_t strbuf_get(Strbuf *restrict sb, int i);
const char* strbuf_bytes(Strbuf *restrict sb);

int strbuf_add(Strbuf *restrict sb, const char *restrict bytes, int at);
int strbuf_rm(Strbuf *restrict sb, int at, int count);

int strbuf_valid(const char *restrict s);

int strbuf_idx(Strbuf *restrict sb, int i);

StrbufBreak strbuf_break(Strbuf *restrict sb, int at);

static inline int strbuf_append(Strbuf *restrict sb, const char *restrict bytes)
{
    return strbuf_add(sb, bytes, strbuf_len(sb) - 1);
}

static inline int strbuf_prepend(Strbuf *restrict sb, const char *restrict bytes)
{
    return strbuf_add(sb, bytes, 0);
}

static inline void strbuf_conv(uint32_t from, char *restrict rune)
{
    int i = 0;

    if (from >= 0xF0000000) {
        rune[i++] = 0x000000FF & (from >> 24);
    }
    if (from >= 0x00E00000) {
        rune[i++] = 0x000000FF & (from >> 16);
    }
    if (from >= 0x0000C000) {
        rune[i++] = 0x000000FF & (from >> 8);
    }
    rune[i++] = from >> 8;

    rune[i] = '\0';
}

#endif /* INNC_STRBUF_H_ */
