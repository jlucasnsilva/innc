/*
    Copyright (c) 2017 João Lucas Nunes e Silva
 
    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.
 
    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
 
    1. The origin of this software must not be misrepresented; you must not
       claim that you wrote the original software. If you use this software
       in a product, an acknowledgment in the product documentation would be
       appreciated but is not required.
    2. Altered source versions must be plainly marked as such, and must not be
       misrepresented as being the original software.
    3. This notice may not be removed or altered from any source distribution.
*/
#include "log.h"
#include "macros.h"

#include <stdio.h>
#include <string.h>
#include <time.h>


static inline void print_prefix(const Logger *restrict l)
{
    time_t    t;
    struct tm ti;
    char      st[256] = "";

    time(&t);
    ti = *localtime(&t);
    strftime(st, 256, "", &ti);

    fprintf(stderr, "%s: ", l->prefix);
    
    if ((l->flags & log_Flag_DATE) || (l->flags & log_Flag_TIME)) {
        if ((l->flags & log_Flag_DATE) && (l->flags & log_Flag_TIME)) {
            strftime(st, 256, "%a %d/%b/%Y %T", &ti);
        } else if (l->flags & log_Flag_DATE) {
            strftime(st, 256, "%a %d/%b/%Y", &ti);
        } else if (l->flags & log_Flag_TIME) {
            strftime(st, 256, "%T", &ti);
        }

        fprintf(stderr, "%s ", st);
    }

    if (l->flags & log_Flag_FILE) {
        fprintf(stderr, "%s:%s:%d - ", __FILE__, __FUNCTION__, __LINE__);
    }
}

void log_fmtd(const Logger *restrict l, const char *restrict fmt, ...)
{
    print_prefix(l);

    va_list args;
    va_start(args, fmt);
    vfprintf(stderr, fmt, args);
    va_end(args);
}

Logger log_make(int flags, bool active, const char *restrict prefix)
{
    Logger l;
    l.flags  = flags;
    l.active = active;
    strncpy(l.prefix, prefix, 23);
    return l;
}
