/** \file */
/*
    Copyright (c) 2017 João Lucas Nunes e Silva
 
    This software is provided 'as-is', without any express or implied
    warranty. In no event will the authors be held liable for any damages
    arising from the use of this software.
 
    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
 
    1. The origin of this software must not be misrepresented; you must not
       claim that you wrote the original software. If you use this software
       in a product, an acknowledgment in the product documentation would be
       appreciated but is not required.
    2. Altered source versions must be plainly marked as such, and must not be
       misrepresented as being the original software.
    3. This notice may not be removed or altered from any source distribution.
*/
#ifndef INNC_FROM_STR_H_
#define INNC_FROM_STR_H_


#include <stdbool.h>
#include <stdlib.h>


#define FROMSTR_SPACE    " \f\n\r\t\v"
#define FROMSTR_WORD1    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
#define FROMSTR_WORD     (FROMSTR_WORD1 "0123456789")
#define FROMSTR_QUOTE    "'\"`"
#define FROMSTR_OPERATOR "+-*/=<>%!&|?:"


extern char* fromstr_spacechars;
extern char* fromstr_wordfstchars;
extern char* fromstr_wordchars;
extern char* fromstr_quotechars;
extern char* fromstr_opchars;


char* strtoword(const char *restrict s, char** saveptr);
char* strtostr(const char *restrict s, char** saveptr);
char* strwhile(const char *restrict s, const char *restrict accept, char** saveptr);
char* struntil(const char *restrict s, const char *restrict brk, char** saveptr);


bool strtonword(const char *restrict s, char** saveptr, char *restrict out, size_t maxlen);
bool strtonstr(const char *restrict s, char** saveptr, char *restrict out, size_t maxlen);
bool strnwhile(const char *restrict s, const char *restrict accept, char** saveptr, char *restrict out, size_t maxlen);
bool strnuntil(const char *restrict s, const char *restrict brk, char** saveptr, char *restrict out, size_t maxlen);


static inline char* strtoop(const char *restrict s, char** saveptr)
{
    return strwhile(s, fromstr_opchars, saveptr);
}

static inline bool strtonop(const char *restrict s, char** saveptr, char *restrict op, size_t maxlen)
{
    return strnwhile(s, fromstr_opchars, saveptr, op, maxlen);
}

const char* dropspaces(const char *restrict s);


#endif // !INNC_FROM_STR_H_
